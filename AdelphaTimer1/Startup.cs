﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdelphaTimer1.Startup))]
namespace AdelphaTimer1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
