create database adelphaTimer

USE adelphaTimer 

create table Ressources(Id_res int primary key not null,Nom_res varchar(20),Prenom_res varchar(20),Num_res varchar(20),Login_res varchar(20),Psw_res varchar(20),Date_naiss_res date, Date_recrut_res date)
create table Teams(Id_team int primary key not null, Nom_team varchar(20) )
create table Ressources_Teams(Id_res int foreign key REFERENCES Ressources(Id_res),Id_team int foreign key REFERENCES Teams(Id_team))
create table Projects(Id_p int primary key not null, Nom_p varchar(20),Desc_p varchar(20),Date_debut_p date,Date_fin_p date,Charge_Totale_p varchar(20),MAJ_p date,Charge_conso_p varchar(20),Charge_rest_p varchar(20),Avancement varchar(20),ETP_p varchar(20),Ecart_charge_p varchar(20),Ecart_duree_p varchar(20),Etat_p varchar(20),Dossier_p varchar(20))
create table Roles(Id_r int primary key not null, Desc_r varchar(20))
create table Ressources_Projects(Id_res int foreign key REFERENCES Ressources(Id_res), Id_p int foreign key REFERENCES Projects(Id_p), Id_r int foreign key REFERENCES Roles(Id_r))
create table Roles_App(Id_ra int primary key not null, Desc_ra varchar(20), Actif_ra bit DEFAULT '1')
create table Categories(Id_c int primary key not null, Desc_c varchar(20), Actif_c bit DEFAULT '1')
create table Sprints(Id_sp int primary key not null, Id_p_sp int foreign key REFERENCES Projects(Id_p),Id_res_sp int foreign key REFERENCES Ressources(Id_res), Date_deb_sp date, Date_fin_sp date, Etat_sp varchar(50), Actif_sp bit DEFAULT '1')
create table Stories(Id_st int primary key not null,Nom_st varchar(20), Id_res_st int foreign key REFERENCES Ressources(Id_res),Importance_st varchar(20), Estim_init_st varchar(20), Demontrer_st varchar(20), Note varchar(20),Id_cat_st int foreign key REFERENCES Categories(Id_c),Demande_par_st varchar(20), Demande_le_st date,Id_p_st int foreign key REFERENCES Projects(Id_p), Id_sp_st int foreign key REFERENCES Sprints(Id_sp), Actif_st bit DEFAULT '1')
/*create table test(id int not null,id_t int foreign key REFERENCES Teams(Id_team), PRIMARY KEY (id, id_t))*/
create table Sponsor(Id_spon int primary key not null,Desc_spon varchar(20), Actif_spon bit DEFAULT '1')
create table Tasks(Id_ta int not null, Id_st_ta int foreign key REFERENCES Stories(Id_st), Nom_ta varchar(20), Desc_ta varchar(20),Id_res_ta int foreign key REFERENCES Ressources(Id_res), D_D_est_ta date, D_F_est_ta date, Travail_est_ta varchar(10),D_D_ta date, D_F_ta date, Travail_ta varchar(10), Id_spon_ta int foreign key REFERENCES Sponsor(Id_spon), Valide_le_ta date, PRIMARY KEY (Id_ta, Id_st_ta), Actif_ta bit DEFAULT '1')


alter table Ressources add Actif_res bit DEFAULT '1'
alter table Teams add Actif_team bit DEFAULT '1'
alter table Ressources_Teams add Actif_res_team bit DEFAULT '1'
alter table Projects add Actif_p bit DEFAULT '1'
alter table Roles add Actif_r bit DEFAULT '1'
alter table Ressources_Projects add Actif_res_p bit DEFAULT '1'
alter table Ressources add Id_ra_res int foreign key REFERENCES Roles_App(Id_ra)
alter table Ressources_Projects add  Id_res_proj int primary key
alter table Ressources_Teams add Id_res_team int primary key

insert into  Roles_App values(1,'Admin')
insert into  Roles_App values(2,'User')

insert into Ressources values(1,	'Belghali',	'Ouafae',	'066522133',	'obelghali',	'123456789',	1993-02-02,	2017-05-14,	1)
insert into Ressources values(2,	'BAABIT',	'Zakaria',	'065654258',	'zbaabit',	'32154584',	1991-02-12,	2016-05-14,	2)